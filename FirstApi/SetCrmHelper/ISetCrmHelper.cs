﻿using SetCrmHelper.MayaModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SetCrmHelper
{
    public interface ISetCrmHelper
    {
        Task<RecordResponse> PostRecordAsync(RecordRequestParameters input);
    }
}
